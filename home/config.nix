/* to be placed in ~/.config/nix */

{
  /* allow non-free packages to be installed */
  allowUnfree = true;
}
