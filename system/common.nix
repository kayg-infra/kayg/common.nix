{ config, lib, pkgs, ... }:

let
  unstable = import <nixos-unstable> {};
in

{
  /* Set the console font and keyboard layout. */
  console = {
    font   = "ter-v32b";
    keyMap = "us";
  };

  /* Set the default locale. */
  i18n = {
    defaultLocale = "en_US.UTF-8";
  };

  /* I'm a big fan of Network Manager but I feel that its use only extends to home
  machines and not on servers so as a rule of experience (I've had servers boot
  up without connected to the internet), I leave it disabled for servers.

  All my services are run inside containers with an internal network that allows
  them to communicate. I, then, use a reverse proxy like Nginx, Caddy or Traefik
  to make them accessible publicly. As I do not open any ports, I realistically
  have no use of a firewall but if I remember correctly, Docker uses IPTables to
  provided network isolation and I do not use Docker on home machines so I have
  it enabled on servers only.

  Additionally, I use a mix of Quad9's EDNS + DNSSEC enabled and OpenDNS' DNSSEC
  enabled nameservers. The plan is to switch to a DNS-over-TLS setup soon. */
  networking = {
    networkmanager.enable = lib.mkDefault false;
    firewall.enable = lib.mkDefault false;
    nameservers = [
      /* Quad9 DNS */
      /* ipv4 */
      "9.9.9.11"
      "149.112.112.11"
      /* ipv6 */
      "2620:fe::11"
      "2620:fe::fe:11"
      /* OpenDNS */
      /* ipv4 */
      "208.67.222.222"
      "208.67.220.220"
      /* ipv6 */
      "2620:119:35::35"
      "2620:119:53::53"
    ];
  };

  /* Optimizing the Nix Store (=/nix/store=) results in saving disk space. This
  is done by hard-linking identical files and symlinks. Files are considered
  identical if they have the same _NAR archive serialisation_ which is just a
  fancy way of saying that they should have the exact contents and permissions.
  Symlinks are considered identical if they have the same content.

  Nix should use all possible cores while building a derivation. Since all of my
  machines have decent to great CPUs, maximum utilization isn't an issue even if
  it continues for a longer interval.

  When an user or the superuser uninstalls something, typically via =nix-env=;
  Nix removes the symlinks from their profile, i.e makes the derivations
  inaccessible to the user requesting their removal. Garbage collection is when
  Nix actually deletes the uninstalled content. */
  nix = {
    autoOptimiseStore = true;
    buildCores = 0;
    gc.automatic = true;
  };

  /* Allow non-free packages to installed. This is needed for packages like the
  nVidia driver. */
  nixpkgs.config = {
    allowUnfree = true;
  };

  /* Change the CPU governor to performance on all other machines. */
  powerManagement.cpuFreqGovernor = "performance";

  /* I run SSH on a non-standard port to mitigate potential brute forcing. I
  also disallow login as root or via passwords. Encrypted keyfiles are the only login methods allowed. */
  services = {
    openssh = {
      enable = true;
      allowSFTP = true;
      passwordAuthentication = false;
      permitRootLogin = "no";
      ports = [ 897 ];
    };
  };

  # enable a cron daemon
  services.fcron.enable = true;

  /* ~Since everything in NixOS is atomic, automatic upgrades on servers is
  something I shouldn't be afraid of. Since I seldom care to upgrade packages
  manually, I would much prefer them to be happening in the background, once
  per day.~

  The above is no longer the case as autoUpgrade changes files when applications
  are using them (for example, the Docker socket). I run `nixos-rebuild switch
  --upgrade` before rebooting anyways so this change shouln't be much of a
  problem. */
  system.autoUpgrade = {
    enable = false;
    channel = "https://nixos.org/channels/nixos-20.09";
  };

  /* I use Docker extensively and exclusively for my personal stack. However, I
  have no use for it on home machines. */
  virtualisation = {
    docker = {
      enable = lib.mkDefault true;
    };
  };

  /* This should only be changed if the user is confident that all the stateful
  changes are compatible with the changed version. Changing the version does not
  upgrade the system. */
  system.stateVersion = "20.09";

  # enable ipv6 for docker
  virtualisation.docker.extraOptions = "--ipv6 --fixed-cidr-v6=fd00::/80";
}
