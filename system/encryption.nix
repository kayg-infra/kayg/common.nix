{ config, lib, pkgs, ... }:

let
  unstable = import <nixos-unstable> {};
in

{
  /* Enable networking in the initramfs image for remote unlocking of encrypted
  drives. */
  boot.initrd.network = {
    enable = true;
    ssh = {
      enable = true;

      /* The path on the kexec image differs from the path on the installed
      system. The following checks both paths and uses one that exists. This
      works because only one of them can exist at at time. */
      hostKeys = [
        "/etc/secrets/initrd/ssh_host_ed25519_key"
      ];
      port = 896;
    };

    /* Wait infinitely for a DHCP lease */
    udhcpc.extraArgs = [ "-t 0" ];
  };
}
