{ config, lib, pkgs, ... }:

{
  /* Enable ZFS support */
  boot = {
    supportedFilesystems = [ "zfs" ];
    zfs = {
      enableUnstable = false;
      forceImportAll = false;
      forceImportRoot = false;
    };
  };

  /* Use ZFS storage driver */
  virtualisation.docker.storageDriver = "zfs";
}
