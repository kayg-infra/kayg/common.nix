{ config, lib, pkgs, ... }:

{
  boot = {
    loader = {
      generationsDir = {
        enable = false;
        copyKernels = true;
      };

      /* I'm usually a rEFInd or EFISTUB guy, preferring the former when I'm too
         lazy to do the latter (I reinstall very often, still don't know why).
         Since, both aren't available on NixOS (I wish I knew enough Nix to
         write a derivation for rEFInd) and I find systemd-boot too minimal for
         my tastes. GRUB is the only choice left.

         - Permission is given to the efibootmgr to modify EFI variables and
           location for the ESP (EFI System Partition) is set to /boot/efi.

         - GRUB is enabled (built with ZFS support).

         - Kernel images and initramfs is copied to the Boot Drive so that GRUB
           does not have to rely on the encrypted pool. */
      grub = {
        enable = true;
        memtest86.enable = true;
        copyKernels = true;
      };
    };

    /* Enable BBR module */
    kernelModules = [ "tcp_bbr" ];
    kernel.sysctl = {
      /* Tuning */

      /* MEMORY */
      /* Refer https://www.kernel.org/doc/Documentation/sysctl/vm.txt
      Refer https://lonesysadmin.net/2013/12/11/adjust-vm-swappiness-avoid-unneeded-disk-io/
      Refer https://lonesysadmin.net/2013/12/22/better-linux-disk-caching-performance-vm-dirty_ratio

      How aggressively should memory pages be written to disk (swapping) */
      "vm.swappiness" = 1;

      /* Tendency of the kernel to reclaim memory which is used in caching of
      directory and inode objects (VFS Cache) */
      "vm.vfs_cache_pressure" = 50;

      /* The absolute maximum amount of system memory that can be filled with
      dirty pages before everything must get committed to disk. Filesystem cache
      based on % of total available memory */
      "vm.dirty_ratio" = 4;

      /* The percentage of system memory that can be filled with “dirty” pages —
       memory pages that still need to be written to disk — before the
       pdflush/flush/kdmflush background processes kick in to write it to disk.

       Minimal % of dirty memory before background PDFLUSH. */
      "vm.dirty_background_ratio" = 2;

      /* FILESYSTEM */
      /* Denotes the maximum number of file-handles the kernel can allocate */
      "fs.file-max" = 100000;

      /* https://wiki.archlinux.org/index.php/Security#Kernel_hardening
       Restricting access to kernel logs */
      "kernel.dmesg_restrict" = 1;

      /* ptrace scope */
      "kernel.yama.ptrace_scope" = 1;

      /* Disable kexec */
      "kernel.kexec_loaded_disabled" = 1;

      # Disable magic SysRq key
      "kernel.sysrq" = 0;

      /* NETWORK */
      /* Security */

      # Ignore ICMP broadcasts to avoid participating in Smurf attacks
      "net.ipv4.icmp_echo_ignore_broadcasts" = 1;

      # Ignore bad ICMP errors
      "net.ipv4.icmp_ignore_bogus_error_responses" = 1;

      # Reverse-path filter for spoof protection
      "net.ipv4.conf.default.rp_filter" = 1;
      "net.ipv4.conf.all.rp_filter" = 1;

      # SYN flood protection
      "net.ipv4.tcp_syncookies" = 1;

      # Do not accept ICMP redirects (prevent MITM attacks)
      "net.ipv4.conf.all.accept_redirects" = 0;
      "net.ipv4.conf.default.accept_redirects" = 0;
      "net.ipv4.conf.all.secure_redirects" = 0;
      "net.ipv4.conf.default.secure_redirects" = 0;
      "net.ipv6.conf.all.accept_redirects" = 0;
      "net.ipv6.conf.default.accept_redirects" = 0;

      # Do not send ICMP redirects (we are not a router)
      "net.ipv4.conf.all.send_redirects" = 0;

      # Do not accept IP source route packets (we are not a router)
      "net.ipv4.conf.all.accept_source_route" = 0;
      "net.ipv6.conf.all.accept_source_route" = 0;

      # Protect against tcp time-wait assassination hazards
      "net.ipv4.tcp_rfc1337" = 1;

      # TCP Fast Open (TFO)
      "net.ipv4.tcp_fastopen" = 3;

      # Number of times SYNACKs for passive TCP connection.
      "net.ipv4.tcp_synack_retries" = 2;

      # Decrease the time default value for tcp_fin_timeout connection
      "net.ipv4.tcp_fin_timeout" = 15;

      # Decrease the time default value for connections to keep alive How often
      # TCP sends out keepalive messages when keepalive is enabled.
      "net.ipv4.tcp_keepalive_time" = 300;

      # How many keepalive probes TCP sends out, until it decides that the
      # connection is broken.
      "net.ipv4.tcp_keepalive_probes" = 5;

      # How frequently the probes are send out. Multiplied by
      # tcp_keepalive_probes it is time to kill not responding connection, after
      # probes started.
      "net.ipv4.tcp_keepalive_intvl" = 15;

      /* Performance */
      ## Bufferbloat mitigations
      # Requires >= 4.9 & kernel module
      "net.ipv4.tcp_congestion_control" = "bbr";

      # Requires >= 4.19
      "net.core.default_qdisc" = "cake";

      # Default Socket Receive Buffer
      "net.core.rmem_default" = 16777216;

      # Maximum Socket Receive Buffer
      "net.core.rmem_max" = 16777216;

      # Default Socket Send Buffer
      "net.core.wmem_default" = 16777216;

      # Maximum Socket Send Buffer
      "net.core.wmem_max" = 16777216;

      # Increase number of incoming connections
      "net.core.somaxconn" = 8192;

      # Increase number of incoming connections backlog
      "net.core.netdev_max_backlog" = 65536;

      # Increase the maximum amount of option memory buffers
      "net.core.optmem_max" = 25165824;

      # Increase the maximum total buffer-space allocatable This is measured in
      # units of pages (4096 bytes)
      "net.ipv4.tcp_mem" = "65536 131072 262144";
      "net.ipv4.udp_mem" = "65536 131072 262144";

      # Increase the read-buffer space allocatable
      "net.ipv4.tcp_rmem" = "8192 87380 16777216";
      "net.ipv4.udp_rmem_min" = 16384;

      # Increase the write-buffer-space allocatable
      "net.ipv4.tcp_wmem" = "8192 65536 16777216";
      "net.ipv4.udp_wmem_min" = 16384;

      # Increase the tcp-time-wait buckets pool size to prevent simple DOS attacks
      "net.ipv4.tcp_max_tw_buckets" = 1440000;
      "net.ipv4.tcp_tw_recycle" = 1;
      "net.ipv4.tcp_tw_reuse" = 1;

      # Tells the system whether it should start at the default window size only
      # for new TCP connections or also for existing connections that have been
      # idle too long
      "net.ipv4.tcp_slow_start_after_idle" = 0;
    };

    /* Mount RAM as tmpfs at /tmp. */
    tmpOnTmpfs = true;
    cleanTmpDir = true;
  };
}
