{ config, lib, pkgs, ... }:

{
  /* I'm a big fan of Network Manager but I feel that its use only extends to home
  machines and not on servers so as a rule of experience (I've had servers boot
  up without connected to the internet), I leave it disabled for servers.

  All my services are run inside containers with an internal network that allows
  them to communicate. I, then, use a reverse proxy like Nginx, Caddy or Traefik
  to make them accessible publicly. As I do not open any ports, I realistically
  have no use of a firewall but if I remember correctly, Docker uses IPTables to
  provided network isolation and I do not use Docker on home machines so I have
  it enabled on servers only.

  Additionally, I use a mix of Quad9's EDNS + DNSSEC enabled and OpenDNS' DNSSEC
  enabled nameservers. The plan is to switch to a DNS-over-TLS setup soon. */
  networking = {
    networkmanager.enable = lib.mkDefault false;
    firewall.enable = lib.mkDefault false;
    nameservers = [
      /* Quad9 DNS */
      /* ipv4 */
      "9.9.9.11"
      "149.112.112.11"
      /* ipv6 */
      "2620:fe::11"
      "2620:fe::fe:11"
      /* OpenDNS */
      /* ipv4 */
      "208.67.222.222"
      "208.67.220.220"
      /* ipv6 */
      "2620:119:35::35"
      "2620:119:53::53"
    ];
  };
}
