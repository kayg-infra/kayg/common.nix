{ config, lib, pkgs, ... }:

{
  /* I use Docker extensively and exclusively for my personal stack. However, I
  have no use for it on home machines. */
  virtualisation = {
    docker = {
      enable = lib.mkDefault true;
    };
  };

  # enable ipv6 for docker
  virtualisation.docker.extraOptions = "--ipv6 --fixed-cidr-v6=fd00::/80 --userland-proxy=false";
}
