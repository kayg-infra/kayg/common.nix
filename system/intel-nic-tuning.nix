{ config, lib, pkgs, ... }:

{
  networking.localCommands = ''
    ${pkgs.ethtool}/bin/ethtool -s eno1 speed 1000 duplex full autoneg off
    ${pkgs.ethtool}/bin/ethtool -K eno1 tx off rx off
    ${pkgs.ethtool}/bin/ethtool -G eno1 rx 4096 tx 4096
    '';
}
