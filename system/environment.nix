{ config, lib, pkgs, ... }:

let
  unstable = import <nixos-unstable> {};
in

{
  environment = {
    /* Packages to be installed system-wide, all machines-wide. */
    systemPackages = with pkgs; [
      bind
      binutils
      coreutils
      curl
      docker-compose
      git
      hdparm
      home-manager
      htop
      lsof
      killall
      neovim
      nixos-generators
      nox
      p7zip
      pciutils
      rsync
      smartmontools
      strace
      tcpdump
      tcptrack
      tree
      unzip
      wget
    ];

    /* Variables to be set system-wide. */
    variables = {
      EDITOR = "nvim";
      VISUAL = "nvim";
    };
  };
}
